# README

 ## Task: Desing and adsd Stock model
 
* Attributes name , ticket_symbol and price.

* Automate looking up stock.

* Automate API key insertion ( instead og having to key it in everytime we look up a stock)

* This will expose us to secure credentials in Rails apps:

* Creadentials.yml.enc (encrypted file)

* Master.key (key to decrypt creadentials file)


* Ruby version 2.5.1

* Rails version 6.0.1

* Database: Sqlite3 development , PostgreSQL production
